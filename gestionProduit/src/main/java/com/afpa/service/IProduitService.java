package com.afpa.service;

import java.util.List;
import java.util.Optional;

import com.afpa.dto.ProduitDto;

public interface IProduitService {

	public List<ProduitDto> chercherToutesLesProduits();
	public boolean supprimerId(int id);
	public Optional<ProduitDto> findById(int id);
	Integer ajouter(ProduitDto prod);
	public Boolean mettreAJourProduit(ProduitDto produitDto);
	
}
	
