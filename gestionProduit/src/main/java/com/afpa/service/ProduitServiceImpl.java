package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.dao.ProduitRepository;

import com.afpa.dto.ProduitDto;
import com.afpa.entity.Produit;


@Service
public class ProduitServiceImpl implements IProduitService {


	@Autowired
	private ProduitRepository produitRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<ProduitDto> chercherToutesLesProduits() {
		List<ProduitDto> maliste = this.produitRepository.findAll()
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.build())
				.collect(Collectors.toList());
		return maliste;
	}


	@Override
	public boolean supprimerId(int id) {
		if(this.produitRepository.existsById(id)) {
			this.produitRepository.deleteById(id);
			return true;
		}
		return false;
	}



	@Override
	public Optional<ProduitDto> findById(int id) {
		Optional<Produit> prod = this.produitRepository.findById(id);
		Optional<ProduitDto> res = Optional.empty();
		if(prod.isPresent()) {
			Produit p = prod.get();
			ProduitDto prodDto = this.modelMapper.map(p, ProduitDto.class);

			res = Optional.of(prodDto);
		}
		return res;
	}


	@Override
	public Integer ajouter(ProduitDto prod) {
		Produit p = this.modelMapper.map(prod,Produit.class);
		p = this.produitRepository.save(p);
		return p.getId();
	}
	
	
	@Override
	public Boolean mettreAJourProduit(ProduitDto produitDto) {
		Produit produit = Produit.builder()
				.id(produitDto.getId())
				.label(produitDto.getLabel())
				.prix(produitDto.getPrix())
				.quantite(produitDto.getQuantite())
				.build();
		try {
			this.produitRepository.save(produit);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
