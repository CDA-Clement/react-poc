package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Produit;

@Repository
public interface ProduitRepository extends CrudRepository<Produit, Integer> {
	
	
	public List<Produit> findAll();
	

}

